﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenCtr : MonoBehaviour
{
    public RectTransform titlePanel;
    public Button ctrBtn;

    private RectTransform maskParentRect;

    public RectTransform blockRect;
    private GridLayoutGroup _gridLayoutGroup;

    public float changeSpeed = 1f;

    public bool isFold;
    // Start is called before the first frame update
    void Start()
    {
        maskParentRect = GetComponent<RectTransform>();
        _gridLayoutGroup = blockRect.GetComponent<GridLayoutGroup>();
        ctrBtn.onClick.AddListener(OnCtrBtnClick);
    }

    public void OnCtrBtnClick()
    {
        StopAllCoroutines();
        isFold = !isFold;
        StartCoroutine(ChangeRectHeight());
    }
    
    IEnumerator ChangeRectHeight()
    {
        //blockRect锚点的y(锚点在顶部，则y表示上边距离父物体顶部)
        float maxHeight = Math.Abs(blockRect.anchoredPosition.y)
                          + (Math.Abs(blockRect.GetChild(blockRect.childCount - 1).GetComponent<RectTransform>().anchoredPosition.y)+_gridLayoutGroup.cellSize.y/2 + _gridLayoutGroup.padding.bottom);
        
        
        float targetVal = isFold ? titlePanel.rect.height : maxHeight;

        float t = 0;
        while (t<changeSpeed)
        {
            yield return null;
            float value = Mathf.Lerp(maskParentRect.sizeDelta.y, targetVal, t / changeSpeed);
            t += Time.deltaTime;
            //高度改变
            maskParentRect.sizeDelta = new Vector2(maskParentRect.sizeDelta.x, value);
        }

    }
    
}
